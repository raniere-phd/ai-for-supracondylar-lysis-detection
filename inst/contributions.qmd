# Author Contributions

-   Guarantors of integrity of entire study
    -   Raniere Gaia Costa da Silva
-   study concepts/study design
    -   Michael Doube
    -   Raniere Gaia Costa da Silva
-   data acquisition
    -   Raniere Gaia Costa da Silva
-   data analysis/interpretation
    -   Raniere Gaia Costa da Silva
-   manuscript drafting or manuscript revision for important intellectual content
    -   Raniere Gaia Costa da Silva
    -   Michael Doube
-   manuscript revision for important intellectual content
    -   all authors
-   approval of final version of submitted manuscript
    -   all authors
-   agrees to ensure any questions related to the work are appropriately resolved
    -   all authors
-   literature research
    -   Raniere Gaia Costa da Silva
    -   Michael Doube
-   experimental studies
    -   Raniere Gaia Costa da Silva
-   statistical analysis
    -   Raniere Gaia Costa da Silva
-   manuscript editing
    -   all authors

