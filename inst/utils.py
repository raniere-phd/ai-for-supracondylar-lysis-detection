import glob
import os.path

from PIL import Image, ImageDraw

import numpy as np
import tifffile

import matplotlib.pyplot as plt

BBOX_MAP = {
    'Condyles-Metacarpal-III': (230, 159, 0, 100),
    'Fetlock': (86, 180, 233, 100),
    'Lateral Sesamoid': (204, 121, 167, 100),
    'Medial Sesamoid': (0, 158, 115, 100),
    'Proximal Phalanx': (213, 94, 0, 100),
}

def tiff2png(radiograph_tiff_path, radiograph_png_path):
    for tiff_filename in glob.glob("*.tiff", root_dir=radiograph_tiff_path, recursive=True):
        im = Image.open(os.path.join(radiograph_tiff_path, tiff_filename))
        im.save(
            os.path.join(radiograph_png_path, tiff_filename.replace("tiff", "png")),
            format="PNG",
        )
        im.close()

def png2rgb(input_filename):
    tiff_filename = input_filename.replace('+png', '').replace('.png', '.tiff')
    
    return tiff2rgb(tiff_filename)
        
def tiff2rgb(input_filename):
    array = tifffile.imread(input_filename)
    array = (array / 65535) * 255
    array = array.round().astype("uint8")

    im = Image.fromarray(array, mode="L")
    im = im.convert("RGB")
    
    return im

def coco2pil(xy):
    x, y, width, height = xy
    
    return (x, y, x + width, y + height)

def get_bbox(filename, coco_json):
    radiograph_info = None
    radiograph_bounding_boxes = []
    
    for idx in range(len(coco_json['images'])):
        if coco_json['images'][idx]['file_name'] == filename:
            radiograph_info = coco_json['images'][idx]

    for idx in range(len(coco_json['annotations'])):
        if coco_json['annotations'][idx]['image_id'] == radiograph_info['id']:
            radiograph_bounding_boxes.append(coco_json['annotations'][idx])
    
    return radiograph_bounding_boxes

def draw_bbox(filename, coco_json, cmap=None, radiograph_tiff_path='', save_as=None):
    radiograph_full_filename = os.path.join(radiograph_tiff_path, filename).replace('+png', '').replace('.png', '.tiff')

    if cmap is None:
        cmap = {item['id']:BBOX_MAP[item['name']] for item in coco_json['categories']}
        
    radiograph_bounding_boxes = get_bbox(filename, coco_json)
    
    im = tiff2rgb(radiograph_full_filename)

    im_with_draw = im.copy()
    draw = ImageDraw.Draw(im_with_draw, 'RGBA')

    for bbox in radiograph_bounding_boxes:
        draw.rectangle(coco2pil(bbox['bbox']), fill=cmap[bbox['category_id']], width=0)

    if save_as is not None:
        im_with_draw.save(save_as)
    
    _ = plt.imshow(im_with_draw)
    

def yolo2pillow(bounding_box, im):
    label, x, y, w, h = bounding_box
    width, height = im.size
    return (
        (x - w / 2) *  width,
        (y - h / 2) * height,
        (x + w / 2) *  width,
        (y + h / 2) * height
    )
    
def draw_yolo_bbox(filename, annotation_filename, cmap=None, radiograph_tiff_path='', save_as=None):
    radiograph_full_filename = os.path.join(radiograph_tiff_path, filename).replace('+png', '').replace('.png', '.tiff')
    
    if cmap is None:
        cmap = list(BBOX_MAP.values())

    radiograph_bounding_boxes = []
    with open(annotation_filename) as _file:
        for line in _file:
            bbox = [float(s) for s in line.split()]
            bbox[0] = int(bbox[0])
            radiograph_bounding_boxes.append(bbox)
    
    im = tiff2rgb(radiograph_full_filename)

    im_with_draw = im.copy()
    draw = ImageDraw.Draw(im_with_draw, 'RGBA')

    for bbox in radiograph_bounding_boxes:
        draw.rectangle(yolo2pillow(bbox, im), fill=cmap[bbox[0]], width=0)

    if save_as is not None:
        im_with_draw.save(save_as)
    
    _ = plt.imshow(im_with_draw)