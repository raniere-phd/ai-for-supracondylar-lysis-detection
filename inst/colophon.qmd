---
engine: knitr
---

# Colophon {.unnumbered}

```{r}
#| echo: false
#| output: false

devtools::load_all()
library(tidyverse)
library(knitr)
```

This document was authored using [Quarto](https://quarto.org/) `r system2("quarto", "--version", stdout = TRUE)`.

Part of this document used [R](https://www.r-project.org/):

```{r}
#| echo: false
#| output: true

sessionInfo()
```
